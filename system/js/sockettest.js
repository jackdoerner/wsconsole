var connection = null;
var statsUpdate = 200;
var stats;
var scrolling;

function statusMessage(head, body, color) {
	var elem = $("#messages");
	var scrolldown = (elem[0].scrollHeight - elem.scrollTop() == elem.outerHeight());
	$('#messages').append('<div class="message"><span class="messageheader" '
			+ ((typeof color == 'undefined') ? '' : ('style="color:' + color + ';"'))
			+ '>' + head + ':</span>&nbsp;<span class="messagebody">'
			+ body + '</span></div>');
	if (scrolldown || scrolling) {
		scrolling = true;
		elem.stop(true).animate({ scrollTop: elem[0].scrollHeight}, 500, function(){
			scrolling = false;
			elem.children().slice(0, -1000).remove();
		});
	}
}

function roundToTwo(bytes) {
	return Math.round(bytes * 100) / 100;
}

function parseHHMMSS(seconds) {
	var ss = seconds % 60;
	var mm = Math.floor(seconds/60) % 60;
	var hh = Math.floor(seconds/3600);
	return ((hh < 10)?('0' + hh):(hh)) + ":" + ((mm < 10)?('0' + mm):(mm)) + ":" + ((ss < 10)?('0' + ss):(ss));
}

function parseBytes(bytes) {
	prefixes = ['', 'K', 'M', 'G', 'T']
	for (var ii in prefixes) {
		if (bytes / 1000 < 1) {
			return roundToTwo(bytes) + prefixes[ii]; 
		} else {
			bytes /= 1000;
		}
	}
	return roundToTwo(bytes) + "T";
}

$(function(){
	$('input.connection').removeAttr('disabled');
	
	$('button.messageSend').click(function(){
		if (connection !== null) {
			var message = $('input.connectionMessage')[0].value;
			try {
				var messageval = JSON.parse(message);
				statusMessage('Client Message', 
						"<span class='dataSegment'>" + messageval.actionType + '</span>: '
						+ "<span class='dataSegment'>" + messageval.dataType.join(', ') + "</span>"
						+ ' @ '+ "<span class='dataSegment'>" + messageval.location + "</span>");
			} catch (e) {
				statusMessage('Client Message', "<span class='dataSegment' title='" + message + "'>" + message + "</span>");
			}
			connection.send(message);
		}
	});
	
	$('button.connectionToggle').click(function(){
		if (connection === null) {
			
			var statsUpdater = null;
			
			$('input.connectionString').attr('disabled', 'disabled');
			$('button.connectionToggle').text("Disconnect");
			statusMessage("Socket Status", "Connection Initiated", "#CCCC00");
			
			var sockAddr = $('input.connectionString')[0].value;
			
			stats = {
				"start":new Date().getTime(),
				"bytes":0,
				"messages":0,
				"bytes":0
			}
			
			connection = new WebSocket(sockAddr, ['soap', 'xmpp']);

			connection.onopen = function () {
				statusMessage("Socket Status", "Successfully Connected", "#00CC00");
				$('button.messageSend').removeAttr('disabled');
			};

			connection.onclose = function() {
				statusMessage("Socket Status", "Connection Closed", "#CCCC00");
				$('input.connectionString').removeAttr('disabled');
				$('button.connectionToggle').text("Connect");
				$('button.messageSend').attr('disabled', 'disabled');
				clearInterval(statsUpdater);
				connection = null;
			}
			
			connection.onerror = function(){
				statusMessage("Socket Status", "Connection Error", "#CC0000");
				connection.close();
			}
			
			connection.onmessage = function(e) {
				stats.messages++;
				stats.bytes += e.data.length;
				try {
					var data = JSON.parse(e.data);
					var dataval;
					if (data.data_value.timeseries.length > 1) {
						dataval = data.data_value.timeseries.join(", ");
					} else {
						dataval = data.data_value.timeseries[0];
					}
					statusMessage("Server Message",
							"<span class='dataSegment'>" + data.data_type + "</span>"
							+ " @ " + "<span class='dataSegment'>" + data.location + "</span>"
							+ " (<span class='dataSegment'>" + new Date(data.data_value.ts * 1000).toISOString() + "</span>): "
							+ "<span class='dataSegment' title='" + dataval + "'>" + dataval + "</span>");
				} catch (err) {
					statusMessage("Server Message", "<span class='dataSegment' title='" + e.data + "'>" + e.data + "</span>");
				}
			};
			
			statsUpdater = setInterval(function(){
				var currentDuration = new Date().getTime() - stats.start;
				$('.time').text(parseHHMMSS(parseInt(currentDuration / 1000)));
				$('.tmessages').text(stats.messages);
				$('.amessages').text(roundToTwo(stats.messages / (currentDuration / 1000)));
				$('.tbytes').text(parseBytes(stats.bytes));
				$('.abytes').text(parseBytes(stats.bytes / (currentDuration / 1000)));
				$('.abm').text(parseBytes((stats.messages>0)?(stats.bytes / stats.messages):(0)));
			}, statsUpdate);
			
		} else {
			connection.close();
		}
	});
});